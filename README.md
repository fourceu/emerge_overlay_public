# README #

The public FourC emerge overlay. Currently empty. See https://github.com/fourceu/qpid-portage-overlay for the public Qpid overlay.

See https://fourceu.atlassian.net/wiki/spaces/AR/pages/8814614/FourC+Private+Portage+Overlay for access info on the private overlay.